package org.cronosx.y.level;

public class Level10 extends Level
{

	@Override
	public String onProcess(String subject, String content, String sender)
	{
		if(content.toLowerCase().contains("freud") || content.toLowerCase().contains("siegmund"))
			return nextToken(sender);
		else return "To stop bothering you with base64 further on: \r\nhttp://y.cronosx.de/files/cat.png\r\n"
				+ "http://y.cronosx.de/files/xor.png";
	}
	
}
