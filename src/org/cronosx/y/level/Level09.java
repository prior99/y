package org.cronosx.y.level;

public class Level09 extends Level
{
	@Override
	public String onProcess(String subject, String content, String sender)
	{
		if(content.toLowerCase().equals("dad had a headache"))
			return nextToken(sender);
		else 
			return "To stop bothering you with base64 further on: \r\nhttp://y.cronosx.de/files/notes.png";
	}
	
}
