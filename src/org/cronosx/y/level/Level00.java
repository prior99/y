package org.cronosx.y.level;


public class Level00 extends Level
{
	@Override
	public String onProcess(String subject, String content, String sender)
	{
		int number = 517;
		String answer = "";
		try
		{
			/*if(content.replace(" ", "").equals("main();") || content.replace(" ", "").equals("main()"))
			{
				String sha = Processor.levels[1].getNumber() + " - " + Processor.levels[1].getToken(sender);
				answer = 	"char*f=\"char*f=%c%s%c; char* get_subject_of_mail() {return %c" + sha + "%c;}main() {printf(f,34,f,34,34,34,10);}%c\"; char* get_subject_of_mail() {return \"" + sha + "\";}main() {printf(f,34,f,34,34,34,10);}";
			}
			else
			{*/
				//System.out.println(content);
				int i = Integer.parseInt(content);
				if(i > number) answer = i + " is too big";
				else if(i < number) answer = i + " is too small";
				else
				{
						/*answer =  "/*\r\n"
								+ " * Header file for y.c\r\n"
								+ " * y.h\r\n"
								+ " *\r\n"
								+ " *\r\n"
								+ "\r\n"
								+ "int main(); //entry point";*/
					String sha = nextToken(sender);
					answer = 	"char*f=\"char*f=%c%s%c; char* get_subject_of_mail() {return %c" + sha + "%c;}main() {printf(f,34,f,34,34,34,10);}%c\"; char* get_subject_of_mail() {return \"" + sha + "\";}main() {printf(f,34,f,34,34,34,10);}";
				}
			//}
		}
		catch(NumberFormatException e)
		{
			answer = "\"" + content + "\" is not a number!";
		}	
		return answer;
	}
	
}
