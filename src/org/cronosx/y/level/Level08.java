package org.cronosx.y.level;

public class Level08 extends Level
{
	@Override
	public String onProcess(String subject, String content, String sender)
	{
		String s;
		String code = "8QOXQOXQO9OZecdOcXYVdUTOUfUbidXY^WO!&OSXQbQSdUbcOd_OdXUO\\UVd";
		
		if(content.toLowerCase().equals("ha_ha_ha_i_just_shifted_everything_16_characters_to_the_left"))
			s = nextToken(sender);
		else
			s = code + "\r\n\r\nWhoa - good thing I wasn't stabbed 23 times with a dagger....";
		return s;
	}
	
}
