package org.cronosx.y.level;


public class Level02 extends Level
{
	@Override
	public String onProcess(String subject, String content, String sender)
	{
		String erl = "Wer reitet so spät durch Nacht und Wind?\r\n"+
				"Es ist der Vater mit seinem Kind.\r\n"+
				"Er hat den Knaben wohl in dem Arm,\r\n"+
				"Er faßt ihn sicher, er hält ihn warm.\r\n"+
				"\r\n"+
				"Mein Sohn, was birgst du so bang dein Gesicht?\r\n"+
				"Siehst Vater, du den Erlkönig nicht!\r\n"+
				"Den Erlenkönig mit Kron' und Schweif?\r\n"+
				"Mein Sohn, es ist ein Nebelstreif.\r\n"+
				"\r\n"+
				"Du liebes Kind, komm geh' mit mir!\r\n"+
				"Gar schöne Spiele, spiel ich mit dir,\r\n"+
				"Manch bunte Blumen sind an dem Strand,\r\n"+
				"Meine Mutter hat manch gülden Gewand.\r\n"+
				"\r\n"+
				"Mein Vater, mein Vater, und hörest du nicht,\r\n"+
				"Was Erlenkönig mir leise verspricht?\r\n"+
				"Sei ruhig, bleibe ruhig, mein Kind,\r\n"+
				"In dürren Blättern säuselt der Wind.\r\n"+
				"\r\n"+
				"Willst feiner Knabe du mit mir geh'n?\r\n"+
				"Meine Töchter sollen dich warten schön,\r\n"+
				"Meine Töchter führen den nächtlichen Reihn\r\n"+
				"Und wiegen und tanzen und singen dich ein.\r\n"+
				"\r\n"+
				"Mein Vater, mein Vater, und siehst du nicht dort\r\n"+
				"Erlkönigs Töchter am düsteren Ort?\r\n"+
				"Mein Sohn, mein Sohn, ich seh'es genau:\r\n"+
				"Es scheinen die alten Weiden so grau.\r\n"+
				"\r\n"+
				"Ich lieb dich, mich reizt deine schöne Gestalt,\r\n"+
				"Und bist du nicht willig, so brauch ich Gewalt!\r\n"+
				"Mein Vater, mein Vater, jetzt faßt er mich an,\r\n"+
				"Erlkönig hat mir ein Leids getan.\r\n"+
				"\r\n"+
				"Dem Vater grauset's, er reitet geschwind,\r\n"+
				"Er hält in den Armen das ächzende Kind,\r\n"+
				"Erreicht den Hof mit Mühe und Not,\r\n"+
				"In seinen Armen das Kind war tot.";
		
		String cypher = "8 1, 31 6, 18 14, " + "24 22, " + 
				"34 23, 34 24, 34 25, 11 8, 26 16, " + "39 25, " +
				"2 8, 2 9, 28 27, " + "27 10, " + 
				"33 25, 24 23, 24 24, 14 24, 21 9, 1 33, " + "3 3, " + 
				"1 1, 1 2, 1 3, 16 20, 28 19, 31 22, 31 21";
		String s;
		if(content.toLowerCase().equals("erlkoenig") || content.toLowerCase().equals("erlkönig") || 
				(content.toLowerCase().contains("erlk") && content.toLowerCase().contains("nig")))
		{
			s = cypher;
		}
		else if(content.toLowerCase().equals("die leiden des jungen werther"))
			s = nextToken(sender);
		else s = erl;
		return s;
	}
	
}
