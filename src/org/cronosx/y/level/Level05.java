package org.cronosx.y.level;

public class Level05 extends Level
{
	@Override
	public String onProcess(String subject, String content, String sender)
	{
		String base64 = "iVBORw0KGgoAAAANSUhEUgAAAAgAAAABCAIAAABsYngUAAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOwQAADsEBuJFr7QAAABp0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuMTFH80I3AAAAHElEQVQYV2PIZGDIY2AoZmDIZ2DIBbOBIokMDAAuIQNfx6A7vQAAAABJRU5ErkJggg==";
		String s;
		if(content.equals("insomnia"))
			s = nextToken(sender);
		else 
			s = base64;
		return s;
	}
	
}
