package org.cronosx.y.level;

import org.cronosx.y.Y;

public class LevelFinal extends Level
{
	@Override
	public String onProcess(String subject, String content, String sender)
	{
		Y.db.requestNotification(sender);
		return "You just reachd the by now highest implemented level. You will receive an email as soon as there is a new level.";
	}
	
}
