package org.cronosx.y.level;

public class Level06 extends Level
{
	@Override
	public String onProcess(String subject, String content, String sender)
	{
		//I shall close my eyes and dream away
		String s;
		String poem = 
			"John Maynard!\r\n"+
			"\"Wer st John Maynard?\"\r\n"+ //I
			"\"John Maynard warunser Steuermann,\r\n"+//
			"aushielt er, bis er da Ufer gewann,\r\n"+//s
			"er hat uns gerettet, er trägt die Kron',\r\n"+
			"er starb für uns, unsre Liebe sein Lohn.\r\n"+
			"Jon Mynard.\"\r\n"+//ha
			"\r\n"+
			"Die \"Schwabe\" fiegt über denErie-See,\r\n"+//ll 
			"Gisht schäumt um den Bug wie Focken von Schnee;\r\n"+//cl
			"von Detrit fliegt sie nach Buffalo -\r\n"+//o
			"die Herzen aber ind frei und froh,\r\n"+//s
			"und die Passagiere mit Kindern und Frauen\r\n"+//e
			"imDämerlicht schon das Ufer schaun,\r\n"+// m
			"und plaudernd man John Manard heran\r\n"+//y
			"trittalles: \"Wie weit noch, Steuermanne?\"\r\n"+// e
			"Der schaut nach vorn und schaut in die Rund:\r\n"+
			"\"Noch dreyßig Minuten ... Halbe Stunde.\"\r\n"+//ye
			"\r\n"+
			"Alle Herzen sind froh, alles Herzen sind frei -\r\n"+
			"da klingt's aus dem Schiffsraum her wie Schrei,\r\n"+
			"\"Feuer!\" wares, was da klang,\r\n"+//s
			"Einqualm aus Kjüt und Luke drang,\r\n"+//a
			"ein Qualm, dann Flammen lichterloh,\r\n"+
			"und noch zwanzig Minuten bis Buffalo.\r\n"+
			"\r\n"+
			"Und die Passagiere, but gemengt,\r\n"+//n
			"am Bugspriet stehn sie zusammengedrängt,\r\n"+
			"am Bugspriet vorn ist noch Luft und Licht,\r\n"+
			"am Steuer aber lagert sich´s dicht,\r\n"+
			"und ein Jammern wir laut: \"Wo sind wir? wo?\"\r\n"+//d
			"Und noch fünfzehnMinuten bis Buffalo. -\r\n"+// 
			"\r\n"+
			"Der Zugwind wächst, doch die Qualmwolke steht,\r\n"+
			"er Kapitän nach dem Steuer späht,\r\n"+//d
			"er sieht nicht mehr seinen Steuermann,\r\n"+
			"aber durchs Sprachrohr fragt er ran:\r\n"+//r
			"\"Noch da, John Maynard?\"\r\n"+
			"\"Ja,Herr. Ich bin.\"\r\n"+
			"\r\n"+
			"\"Auf den Strand! Ein die Brandung!\"\r\n"+//e
			"\"Ich hlte drauf hin.\"\r\n"+//a
			"Und das Schiffsvolk jubelt: \"Halt aus! Hallo!\"\r\n"+
			"Und noch zehn Minuten bis Buffalo. - -\r\n"+
			"\r\n"+
			"\"Noch da, John Maynard?\" Und Antwort schallt's\r\n"+
			"mit ersterbender Stime:\"Ja, Herr, ich halt's!\"\r\n"+//m
			"Und in die Brandung, wasKlippe, was Stein,\r\n"+// 
			"jagt er die \"Schwlbe\" mitten hinein.\r\n"+//a
			"Soll Rettung kommen, so kommt sie nur so.\r\n"+
			"Rettung: der Strand von Buffalo!\r\n"+
			"\r\n"+
			"Das Schiff geborsten. Das Feuer verschelt.\r\n"+//w
			"Gerettet alle. Nur einer fehlt!\r\n"+
			"\r\n"+
			"Alle Glocken gehn; ihre Töne aschwell'n\r\n"+//a
			"himmelan aus Kirchen und Kapell'n,\r\n"+
			"ein Klingen und Läuten, sonst schweigt die Stadt,\r\n"+
			"ein Dienst nur, den sie heute hat:\r\n"+
			"Zehntausend folgen oder mehr,\r\n"+
			"und kein Aug' im Zuge, das tränenleer.\r\n"+
			"\r\n"+
			"Sie lassen den Sarg in Blumen hinab,\r\n"+
			"mit Blumen schließen sie das Grab,\r\n"+
			"und mit goldner Schrift in den Marmorstein\r\n"+
			"schreibt die Stadt ihren Dankspruch ein:\r\n"+
			"\r\n"+
			"\"Hier ruht John Manard! In Qualm undBrand\r\n"+//y
			"hielt er das Steuer fest in der Hand,\r\n"+
			"er hat uns gerettet, er trägt die Kron,\r\n"+
			"er starb für uns, unsre Liebe sein Lohn.\r\n"+
			"John Maynard.\"\r\n";
		if(content.toLowerCase().equals("i shall close my eyes and dream away"))
			s = nextToken(sender);
		else
			s = poem;
		return s;
	}
	
}
