package org.cronosx.y.level;


public class Level01 extends Level
{
	@Override
	public String onProcess(String subject, String content, String sender)
	{
		String s;
		if(content.toLowerCase().contains("bello") || content.toLowerCase().contains("gallico")) 
			s = "Yes, thats where it's from. But thats not the solution :/";
		else if(content.toLowerCase().contains("caesar"))
			s = "Yep, it's from caesar de bello gallico. But sadly thats not what I am looking for.";
		else if(content.equals("3") || content.toLowerCase().equals("tres")
				|| content.toLowerCase().equals("three"))
			s = nextToken(sender);
		else
			s = "The following is base64\r\n\r\niVBORw0KGgoAAAANSUhEUgAAABgAAAAfCAIAAAByEJoXAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjExR/NCNwAAANVJREFUSEu9jtsWhCAMA/3/n96NBEOo3HRX54EzTVtg+zOfRC6GjMbUW7yrS7gIpyTFJaFL5Bnv3TgrECn1Nr15Evgz9K5XeJYGg97SvuCQj8JD2BR5xnvN8yyhW0CklA5UuugkqfMcfOH8zuWXpwvzGzFB6IOTwL3MKKL0zlAqrEBK6ApdvCQsX+TOm/5VChPAcIm8cewzBO5LhAWVly8C2OFaWA7lW6Tf7OS6w3SgTAxG93cSXsopkyF3CfFwxwtvV0NW9vKqASd0hkJJGmnP/Ma2fQF4bu0hWRY+agAAAABJRU5ErkJggg==";
		return s;
	}
	
}
