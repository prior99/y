package org.cronosx.y.level;


import org.cronosx.y.Answer;
import org.cronosx.y.Processor;
import org.cronosx.y.Y;

public abstract class Level
{
	boolean reached = false;
	
	public void identify()
	{	
		System.out.println("Level " + getClass() + " registered as #"+ getNumber());
	}
	
	public int getNumber()
	{
		return Processor.levels.indexOf(this);
	}
	
	public Answer process(String subject, String content, String sender)
	{
		System.out.println("    LVL:Level " + getNumber());
		Y.db.newMail(Processor.getSHA512(sender), getNumber());
		content = trimBreaks(content.trim()).trim();
		if(!reached)
		{
			reached = true;
			//System.out.println(sender + " reached level " + getNumber());
		}
		return new Answer("Re:" + getToken(sender), onProcess(subject, content, sender));
	}
	
	public abstract String onProcess(String subject, String content, String sender);
	
	public String getToken(String mail)
	{
		return getNumber() + " - " + Processor.getSHA512("Y_Level_" + getNumber() + mail.toLowerCase());
	}
	
	public static String trimBreaks(String s)
	{
		if(s.length() == 0) return s;
		while(s.endsWith("\n") || s.endsWith("\r")) s = s.substring(0, s.length() - 1);
		while(s.startsWith("\n") || s.startsWith("\r")) s = s.substring(1);
		return s;
	}
	
	protected String nextToken(String mail)
	{
		return Processor.levels.get(getNumber() + 1).getToken(mail);
	}
}
