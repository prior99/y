package org.cronosx.y.level;

public class Level07 extends Level
{
	@Override
	public String onProcess(String subject, String content, String sender)
	{
		String s;
		String hex = "57005 48879, 57007 3501, 2989 3053"; //DEAD BEEF DEAF DAD DEAF BED
		if(content.toLowerCase().replace(" ", "").replace(",", "").replace("\r", "").replace("\n", "").equals("deadbeefdeafdadbadbed"))
			s = nextToken(sender);
		else 
			s = hex;
		return s;
	}
	
}


