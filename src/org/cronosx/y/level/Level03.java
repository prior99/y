package org.cronosx.y.level;


public class Level03 extends Level
{
	@Override
	public String onProcess(String subject, String content, String sender)
	{
		String s;
		String palindrom = "10301 10501 10601 11311\r\n" + 
				"11411 12421 12721 12821\r\n" +
				"13331 13831 13931 14341\r\n" +
				"14741 15451 15551 16061\r\n" +
				"16361 16561 16661 17471\r\n" +
				"17971 18181 18481 19391\r\n" +
				"19891 19991 30103 30203\r\n" +
				"30403 30703 30803 31013\r\n" +
				"31513 32323 32423 33533\r\n" +
				"34543 34843 35053 35153\r\n" +
				"35353 35753 36263 36563\r\n" +
				"37273 37573 38083 38183\r\n" +
				"38783 39293 70207 70507\r\n" +
				"70607 71317 71917 72227\r\n" +
				"72727 73037 73237 73637\r\n" +
				"74047 74747 75557 76367\r\n" +
				"76667 77377 77477 77977\r\n" +
				"78487 78787 78887 79397\r\n" +
				"79697 79997 90709 91019\r\n" +
				"93139 93239 93739 94049\r\n" +
				"94349 94649 94849 94949\r\n" +
				"95959 96269 96469 96769\r\n" +
				"97379 97579 97879 98389 \r\n";
		if(content.equals("98689"))
			s = nextToken(sender);
		else 
		{
			try
			{
				Integer.parseInt(content);
				s = "Are you just trying random numbers? Are you kidding me?";
			}
			catch(NumberFormatException e)
			{
				s = palindrom;
			}
		}
		return s;
	}
	
}
