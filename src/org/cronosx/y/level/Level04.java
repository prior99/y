package org.cronosx.y.level;


public class Level04 extends Level
{
	@Override
	public String onProcess(String subject, String content, String sender)
	{
		String s;
		String base64 = "iVBORw0KGgoAAAANSUhEUgAAABQAAAAPCAIAAABr+ngCAAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOwgAADsIBFShKgAAAABp0RVh0U29mdHdhcmUAUGFpbnQuTkVUIHYzLjUuMTFH80I3AAAA6UlEQVQ4T2PgqfiPiv4Rj6ipGV0aiHgr/6KJwBGyZnQ5CMqpnu9UfQJNEILwaVaofl1b3XepynJPtX9S9QqJqk9oCrBprgSjin/zanL/V/BA0J8KAfvqMwg1YIRFc1T1hgPV3um1S/5W8ME1A1Fu9Ty4GghCaOav/M1b9dey5tLVajOg0s+VEsg6f1UI6lbdw6kZLhRfveZEtXNu7VwSbEaIwvy8pCYdrvN3hYBj9SmEGjDCphmGFKtftda0XasyPVjtlVW9ULLqI5oCfJohqLB6lmv1MTRBCELWjF0/kSkMp35ciALNFf8APxXFPCeDWo4AAAAASUVORK5CYIJQSwMEFAAAAAgAeUB8Qz90aydCAAAAVwAAAAYAAAByZWFkbWXzVEjMVUhUKMpPztZRgHDyFDKLcxLzUnS4uKL19PRiubiAHKgahbTU1Jxihbx8hYLEzDyQBEK9Ql5qWWqRQjwIAABQSwECPwAUAAAACAB5QHxDP3RrJ0IAAABXAAAABgAkAAAAAAAAACAAAAAAAAAAcmVhZG1lCgAgAAAAAAABABgAojTt/AfszgH+PATXB+zOAf48BNcH7M4BUEsFBgAAAAABAAEAWAAAAGYAAAAAAA==";
		if(content.toLowerCase().equals("cries"))
			s = nextToken(sender);
		else
			s = "\"She closed the zipper to keep her warm even though it felt compressing.\r\n\r\n"
					+ base64;
		return s;
	}
	
}
