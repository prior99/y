package org.cronosx.y;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.cronosx.y.level.*;

public class Processor
{
	private static MessageDigest sha1;
	public static List<Level >levels;
	public static Level prequel = new Prequel();
	
	public void initLevels()
	{
		levels.add(new Level00());
		levels.add(new Level01());
		levels.add(new Level02());
		levels.add(new Level03());
		levels.add(new Level04());
		levels.add(new Level05());
		levels.add(new Level06());
		levels.add(new Level07());
		levels.add(new Level08());
		levels.add(new Level09());
		levels.add(new Level10());
	}
	
	public Processor()
	{
		levels = new ArrayList<Level>();
		initLevels();
		for(Level l : levels)
			l.identify();
		levels.add(new LevelFinal());
		Y.db.updateLevelAmount(levels.size());
		try
		{
			sha1 = MessageDigest.getInstance("SHA-1");
		}
		catch(NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
	}
	
	public Answer process(String subject, String content, String from)
	{
		Answer answer = null;
		boolean found = false;
		//System.out.println(subject);
		if(subject.toLowerCase().startsWith("re: ")) subject = subject.substring(4);
		else if(subject.toLowerCase().startsWith("re:")) subject = subject.substring(3);
		else if(subject.toLowerCase().startsWith("aw: ")) subject = subject.substring(4);
		else if(subject.toLowerCase().startsWith("aw:")) subject = subject.substring(3);
		//System.out.println(subject);
		for(Level l : levels)
		{
			if(subject.equals(l.getToken(from)))
			{
				answer = l.process(subject, content, from);
				if(answer != null)
					found = true;
			}
		}
		if(!found) answer = prequel.process(subject, content, from);
		return answer;
	}
	
	public static String getSHA512(String toEncrypt)
	{
		byte[] enc = sha1.digest(toEncrypt.getBytes());
		String result = "";
		for(int i=0; i < enc.length; i++) 
		{
			result += Integer.toString((enc[i] & 0xff) + 0x100, 16).substring(1);
		}
		return result;
	}

}
