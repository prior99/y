package org.cronosx.y;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Database
{
	private Connection conn;
	public Database()
	{
		createTables();
	}
	
	private void connect() throws SQLException
	{
		conn = DriverManager.getConnection("jdbc:" + Y.dbServer + "/" + Y.dbDatabase +"?autoReconnect=true", Y.dbUser, Y.dbPassword);
	}
	
	private void createTables()
	{
		try
		{
			connect();
			PreparedStatement stmt = conn.prepareStatement("CREATE TABLE IF NOT EXISTS `Mails` ("
					+ "`ID`			INT 			NOT NULL AUTO_INCREMENT PRIMARY KEY,"
					+ "`User`		VARCHAR(40)		NOT NULL,"
					+ "`Level`		INT				NOT NULL,"
					+ "`Timestamp`	INT				NOT NULL)");
			stmt.executeUpdate();
			stmt = conn.prepareStatement("CREATE TABLE IF NOT EXISTS `Levels` ("
					+ "`ID`			INT 			NOT NULL AUTO_INCREMENT PRIMARY KEY,"
					+ "`Level`		INT				NOT NULL,"
					+ "`Timestamp`	INT				NOT NULL)");
			stmt.executeUpdate();
			stmt = conn.prepareStatement("CREATE TABLE IF NOT EXISTS `Requests` ("
					+ "`ID`			INT 			NOT NULL AUTO_INCREMENT PRIMARY KEY,"
					+ "`Mail`		VARCHAR(150)	NOT NULL,"
					+ "`Timestamp`	INT				NOT NULL)");
			stmt.executeUpdate();
			stmt = conn.prepareStatement("CREATE TABLE IF NOT EXISTS `Users` ("
					+ "`ID`			INT 			NOT NULL AUTO_INCREMENT PRIMARY KEY,"
					+ "`User`		VARCHAR(40)		NOT NULL,"
					+ "`Name`		VARCHAR(40)		NOT NULL)");
			stmt.executeUpdate();
			shutdown();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void requestNotification(String mail)
	{
		try
		{
			connect();
			PreparedStatement stmt = conn.prepareStatement("INSERT INTO `Requests` (`Mail`, `Timestamp`) VALUES(?, ?)");
			stmt.setString(1, mail);
			stmt.setInt(2, (int)(System.currentTimeMillis()/1000));
			stmt.executeUpdate();
			shutdown();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void newMail(String user, int level)
	{
		try
		{
			connect();
			PreparedStatement stmt = conn.prepareStatement("INSERT INTO `Mails`"
					+ "(`User`, `Level`, `Timestamp`) VALUES"
					+ "(?, ?, ?)");
			stmt.setString(1, user);
			stmt.setInt(2, level);
			stmt.setInt(3, (int)(System.currentTimeMillis()/1000));
			stmt.executeUpdate();
			shutdown();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void updateLevelAmount(int amount)
	{
		try
		{
			connect();
			PreparedStatement stmt = conn.prepareStatement("SELECT MAX(`Level`) AS MaxLevel FROM `Levels`");
			ResultSet rs = stmt.executeQuery();
			int max; 
			if(rs.first())
				max = rs.getInt("MaxLevel");
			else
				max = -1;
			rs.close();
			if(amount > max)
			{
				PreparedStatement stmt2 = conn.prepareStatement("INSERT INTO `Levels` (`Level`, `Timestamp`) VALUES (?, ?)");
				stmt2.setInt(1, amount);
				stmt2.setInt(2, (int)(System.currentTimeMillis()/1000));
				stmt2.executeUpdate();
			}
			notifyUsers();
			shutdown();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	private void notifyUsers() throws SQLException
	{
		PreparedStatement stmt = conn.prepareStatement("SELECT `Mail` FROM `Requests`");
		ResultSet rs = stmt.executeQuery();
		List<String> mails = new ArrayList<String>();
		while(rs.next())
		{
			mails.add(rs.getString("Mail"));
		}
		rs.close();
		for(String mail : mails)
		{
			System.out.println("Sending new level reminder to " + mail);
			stmt = conn.prepareStatement("DELETE FROM `Requests` WHERE `Mail` = ?");
			stmt.setString(1, mail);
			stmt.executeUpdate();
			SMTP smtp = Y.getNewSMTP();
			smtp.sendMail(mail, "New Level", "A new level was just added to CronosX - Y. \r\nYou received this notification because you previously reached the maximum implemented level.");
		}
	}
	
	private void shutdown()
	{
		try 
		{
			conn.close();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}
}
