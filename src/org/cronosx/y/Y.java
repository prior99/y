package org.cronosx.y;

import java.io.File;
import java.text.*;
import java.util.*;

import org.cronosx.tools.Config;


public class Y
{
	private static String user;
	private static String server;
	private static String password;

	public static String dbServer;
	public static String dbDatabase;
	public static String dbUser;
	public static String dbPassword;
	
	public static Database db;
	
	private Processor proc;
	
	public Y()
	{
		Config config = new Config(new File("server.conf"));
		user = 		config.getStr("mailUser", "y@cronosx.de");
		server = 	config.getStr("mailServer", "cronosx.de");
		password = 	config.getStr("mailPassword", "");
		dbUser = 	config.getStr("dbUser", "y");
		dbServer =	config.getStr("dbServer", "mysql://cronosx.de");
		dbPassword =config.getStr("dbPassword", "");
		dbDatabase =config.getStr("dbDatabase", "y");
		db = new Database();
		proc = new Processor();
	}
	
	public static POP3 getNewPOP3()
	{
		return new POP3(server, 110, user, password);
	}
	
	public static SMTP getNewSMTP()
	{
		return new SMTP(server, 25, user, password, user);
	}
	
	public void start()
	{
		while(true)
		{
			try
			{
				Thread.sleep(5000);	
			}
			catch(Exception e)
			{
				break;
			}
			POP3 connection = getNewPOP3();
			Mail[] mails = connection.fetchMails();
			for(Mail m : mails)
			{
				Date d = new Date();
				SimpleDateFormat sf = new SimpleDateFormat("dd.MM.yyy HH:mm:ss");
				
				System.out.println("[" + sf.format(d) + "] Mail from " + m.getFrom() + " "+ Processor.getSHA512(m.getFrom()) + "\n"
				 + "    SUB:" + m.getSubject() + "\n"
				 + "    CON:" + m.getText().replace("\n", "\n        "));
				
				Answer aw = proc.process(m.getSubject(), m.getText(), m.getFrom());
				SMTP smtp = getNewSMTP();
				smtp.sendMail(m.getFrom(), aw.getSubject(), aw.getContent());
				
			}
			connection.close();
		}
	}
	
	public static void main(String[] args)
	{
		Y y = new Y();
		y.start();
	}
}
