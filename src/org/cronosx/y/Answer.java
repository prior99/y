package org.cronosx.y;

public class Answer
{
	private String subject;
	private String content;
	public Answer(String subject, String content)
	{
		this.subject = subject;
		this.content = content;
	}
	
	public String getContent()
	{
		return content;
	}
	
	public String getSubject()
	{
		return subject;
	}
}
