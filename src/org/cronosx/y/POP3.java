package org.cronosx.y;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class POP3
{
	private String server;
	private int port;
	private String user;
	private String password;
	private Socket socket;
	private PrintWriter out;
	private BufferedReader in;
	
	public POP3(String server, int port, String user, String pass)
	{
		this.server = server;
		this.port = port;
		this.user = user;
		this.password = pass;
		open();
		login();
	}
	
	public Mail[] fetchMails()
	{
		List<Mail> mails= new ArrayList<Mail>();
		List<String> nums= new ArrayList<String>();
		
		send("LIST");
		receive();
		String s;
		
		try
		{
			while(!(s = in.readLine()).equals("."))
			{
				nums.add(s.split(" ")[0]);
			}
			for(String s2 : nums)
				mails.add(readMail(s2));
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
		Mail[] mailsA = new Mail[mails.size()];
		mails.toArray(mailsA);
		return mailsA;
	}
	
	private Mail readMail(String num)
	{
		send("RETR " + num);
		String content = "";
		try
		{
			while(true)
			{
				String s = in.readLine();
				if(s.equals("."))
					break;
				content += s + "\n";
			}
			send("DELE " + num);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return new Mail(content);
	}
	
	private void open()
	{
		try
		{
			socket = new Socket(server, port);
			this.out = new PrintWriter(socket.getOutputStream());
			this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		receive();
	}
	
	private void login()
	{
		send("USER " + user);
		receive();
		send("PASS " + password);
		receive();
	}
	
	public void close()
	{
		try
		{
			goodbye();
			out.close();
			in.close();
			socket.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private void goodbye()
	{
		send("QUIT");
	}
	
	public String receive()
	{
		String s;
		try
		{
			s = in.readLine();
			//System.out.println(s);
			if(!s.substring(0, 3).equals("+OK"))
			{
				System.out.println("ERROR FROM SERVER:" + s);
			}
			if(s.length() > 3)
			{
				s = s.substring(4);
				return s;
			}
			else
				return null;
		}
		catch(IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public void send(String s)
	{
		out.println(s);
		out.flush();
	}
}
