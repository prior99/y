package org.cronosx.y;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.xml.bind.DatatypeConverter;

public class SMTP
{
	private String smtpServer; //Server to connect to
	private String smtpUser; //User to athenticate with
	private String smtpPass; //Password to authenticate with
	private String from; //Origin of the mail
	private int smtpPort; //Port of the server (usually 25)
	private String response; //Responsestring of the server
	private Socket socket;
	private BufferedReader in;
	private PrintWriter out;
	
	/*
	 * Create a new smtpClient instance and store its configuration
	 */
	public SMTP(String server, int port, String user, String pass, String from)
	{
		smtpPort = port;
		this.smtpServer = server; 
		this.smtpUser = DatatypeConverter.printBase64Binary(user.getBytes());
		this.smtpPass = DatatypeConverter.printBase64Binary(pass.getBytes());
		this.from = from;
	}
	
	public void connect()
	{
		try
		{
			socket = new Socket(smtpServer, smtpPort);
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			out = new PrintWriter(socket.getOutputStream());
			response =  receive();
			send("HELO cronosx.de");
			response =  receive();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private void send(String s)
	{
		//System.out.println(">>"+s);
		out.println(s);
		out.flush();
	}
	
	private String receive()
	{
		try
		{
			String s =  in.readLine();
			//System.out.println("<<"+s);
			return s;
		}
		catch(IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	/*
	 * Disconnect and close the socket
	 */
	public void disconnect()
	{
		try
		{
			send("QUIT");
			out.close();
			in.close();
			socket.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/*
	 * Login with the specified data
	 */
	public void login()
	{
		send("auth login"); //Start authentication
		response = receive();
		send(smtpUser);
		response = receive();
		send(smtpPass);
		response = receive();
	}

	/*
	 * Send the specified mail
	 */
	public void sendMail(String to, String subject, String body)
	{
		response = "";
		connect();
		login(); //Login
		send("MAIL FROM: <"+from+">"); //Send mail
		response = receive();
		send("RCPT TO: <"+to+">");
		response = receive();
		send("DATA");
		response = receive();
		send(
			"To: <"+to+">\r\n" +
			"From: "+from+"\r\n" +
			"Subject:"+subject+"\r\n" +
			"Content-Type:text/plain; charset=UTF-8\r\n" + 
			body+"\r\n.\r\n");
		response = receive();
		disconnect();
	}
	
	/*
	 * For debug purposes, get response from server
	 */
	public String getResponse()
	{
		return response; //Get responsestring of last sent mail
	}
}
