package org.cronosx.y;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.SharedByteArrayInputStream;

public class Mail
{
	private String from, text, subject;
	public Mail(String content)
	{
		Session s = Session.getDefaultInstance(new Properties());
		InputStream is = new ByteArrayInputStream(content.getBytes());
		try
		{
			MimeMessage message = new MimeMessage(s, is);
			Object con = message.getContent();
			if(con instanceof MimeMultipart)
			{
				MimeMultipart mm = (MimeMultipart) con;
				for(int i = 0; i < mm.getCount(); i++)
				{
					MimeBodyPart bp = (MimeBodyPart)mm.getBodyPart(i);
					if(bp.isMimeType("text/plain"))
					{
						text = bp.getContent().toString().trim();
					}
				}
			}
			else if(con instanceof String)
			{
				text = con.toString().trim();
			}
			else if(con instanceof SharedByteArrayInputStream)
			{
				InputStreamReader rd = new InputStreamReader((InputStream) con);
				StringBuilder sb = new StringBuilder();
				int i;
				while((i = rd.read()) != -1)
					sb.append((char)i);
				text = sb.toString();
			}
			else
				text = "";
			if(message.getFrom().length >= 1)
			{
				from = message.getFrom()[0].toString().toLowerCase();
				if(from.indexOf('<') != -1)
				{
					from = from.substring(from.indexOf('<')+1, from.indexOf('>'));
				} 
			}
			subject = message.getSubject();
			if(subject == null) subject = "";
		}
		catch(MessagingException e)
		{
			e.printStackTrace();
		}
		catch(IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public String getFrom()
	{
		return from;
	}
	public String getText()
	{
		return text;
	}
	public String getSubject()
	{
		return subject;
	}
}
