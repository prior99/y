<?php 

	/*
	 * 
	 * SET DATABASE CONFIGURATION HERE!
	 * 
	 */
	function mopen()
	{
		$dbServer = 		"cronosx.de";
		$dbDatabase = 		"y";
		$dbUser = 			"y";
		$dbPassword = 		"U7axwxh6mPWRQx86";
		return new mysqli($dbServer, $dbUser, $dbPassword, $dbDatabase);
	}

?>

<!Doctype HTML>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="style.css" />
	<link rel="shortcut icon" href="favicon.png" type="image/png" />
	<link rel="icon" href="favicon.png" type="image/png" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>CronosX -Y</title>
</head>
<body>
<div class="wrapper">
	<div class="container">
		<div class="header">
		</div>
		<div class="navigation">
			<ul>
				<li><a href="?action=about">About</a></li>
				<li><a href="?action=levels">Levels</a></li>
				<li><a href="?action=lookup_user">Lookup User</a></li>
				<li><a href="?action=claim">Claim a Checksum</a></li>
				<li><a href="?action=top_users">Top Users</a></li>
			</ul>
		</div>
	</div>
	<div class="container">
		<?php 
			if($_GET["action"] == "levels") 			levels();
			else if($_GET["action"] == "lookup_user")	lookup_user($_GET["user"]);
			else if($_GET["action"] == "claim")			claim($_GET["user"], $_GET["name"]);
			else if($_GET["action"] == "top_users")		top_users($_GET["page"]);
			else 										about();
		
		?>	
	</div>
</div>
</body>
</html>

<?php 

	function about()
	{
		$mysqli = mopen();
		$stmt = $mysqli -> prepare("SELECT MAX(`Level`) FROM `Levels`");
		$stmt -> execute();
		$stmt -> bind_result($level);
		$stmt -> fetch();
		$stmt -> close();
		$mysqli -> close();
		?>		
			<div class="headline">
				About
			</div>
			<div class="content">
				<p>What is <a href="mailto: y@cronosx.de">y@cronosx.de</a>?</p>
				<p>Y is a game of riddles played entirely by mail.</p>
				<p>I do not want to spoiler too much, so why don't you check it out yourself? Just write a mail to <a href="mailto: y@cronosx.de">y@cronosx.de</a>!</p>
				<p>There are no real gameplay advices. Just try and error. Send mails with different content, play around and see what the server answers to specific input.</p>
				<p>Try basic encryption methods such as xor, base64, Caesar or Vigenere. Experiment around and do not hestitate send mails to the server!</p>
				<p>When you send your first mail, you will receive a prequel which explains more.</p>
				<p>If you are not receiving E-Mails, please check your spamfolder.</p>
				<br />
				<p>There are <b><?php echo($level - 1); ?></b> levels currently implemented - but the content is growing. Try to reach a level first to get your id into the scoreboard.</p>
			</div>
		<?php
	}
	
	function claim($user, $name)
	{
		?>
			<div class="headline">
				Claim Checksum
			</div>
			<div class="content">
				<?php 
					if(!empty($user) && !empty($name))
					{
						$mysqli = mopen();
						$stmt = $mysqli -> prepare("SELECT `Name`, `User` FROM `Users` WHERE `User` = ?");
						$stmt -> bind_param("s", $user);
						$stmt -> execute();
						$stmt -> bind_result($name, $user);
						if($stmt -> fetch())
						{
							?>
								<div class="error">
									<h1>Error</h1>
									<p>Unable to claim! Checksum <?php echo($user);?> was already named <?php echo($name);?></p>
									<p>Checksums may be named just once, names may be assigned just once</p>
								</div>
							<?php 
						}
						else
						{
							$stmt -> close();
							$stmt = $mysqli -> prepare("INSERT INTO `Users` (`User`, `Name`) VALUES (?, ?)");
							$stmt -> bind_param("ss", $user, $name);
							$stmt -> execute();
							?>
								<div class="success">
									<h1>Success</h1>
									<p>Checksum <?php echo($user); ?>was successfully named after <?php echo($name); ?></p>
								</div>
							<?php 
						}
						$stmt -> close();
						$mysqli -> close();
					}
				?>
				<p>This will claim a checksum to be yours. E.g. It will be named after you.</p>
				<p>Example: Instead of 68ac906495480a3404beee4874ed853a037a7a8f there will be the name you enter here in all scoreboards</p> 
				<form method="GET" action="#">
					<input type="hidden" name="action" value="claim" />
					<label>Checksum to claim: </label>
					<input type="text" name="user" />
					<label>Username: </label>
					<input type="text" name="name" />
					<label>Claim</label>
					<input type="submit" value="OK" />
				</form>
			</div>
		<?php 
	}

	function levels()
	{
		$mysqli = mopen();
		$stmt = $mysqli -> prepare("SELECT m.`Level`, IFNULL(u.`Name`, m.`User`), m.`Timestamp` FROM `Mails` m LEFT JOIN `Users` u ON m.`User` = u.`User` GROUP BY `Level` ORDER BY `Level`, `Timestamp`");
		$stmt -> execute();
		$stmt -> bind_result($level, $user, $timestamp);
		
		?>		
			<div class="headline">
				Levels
			</div>
			<div class="content">
				<p>The following table contains all levels that have been reached so far by any user. The first user ever to reach this level will be saved in this table.</p>
				<table>
					<tr class="head">
						<td>Level</td>
						<td>First User ever</td>
						<td>Time</td>
					</tr>
					<?php 
						while($stmt -> fetch())
						{
							$time = date('d.m.Y H:i:s', $timestamp);
							?>
								<tr>
									<td><?php echo($level); ?></td>
									<td><?php echo($user); ?></td>
									<td><?php echo($time); ?></td>
								</tr>
							<?php
						}
					?>
				</table>
			</div>
		<?php
		$mysqli -> close();
	}
	
	function lookup_user($user)
	{
		?>
			<div class="headline">
				Lookup user
			</div>
			<div class="content">
				<?php
					if(!empty($user))
					{
						$mysqli = mopen();
						
						$stmt = $mysqli -> prepare("SELECT `Name`,`User` FROM `Users` WHERE `User` = ? OR `Name` = ?");
						$stmt -> bind_param("ss", $user, $user);
						$stmt -> execute();
						$stmt -> bind_result($name, $user);
						$stmt -> execute();
						$stmt -> fetch();
						$stmt -> close();
						
						if(!empty($user))
						{
							if(empty($name))
							{
								$name = "<i>Unnamed</i>";
							}	
							$stmt = $mysqli -> prepare("SELECT `Level`, MIN(`Timestamp`), COUNT(*) FROM `Mails` WHERE `User` = ? GROUP BY `Level` ORDER BY `Level`");
							$stmt -> bind_param("s", $user);
							$stmt -> execute();
							$stmt -> bind_result($level, $timestamp, $count);
							?>
								<h1>Result</h1>
								<p>All information about the user <b><?php echo($user);?></b></p>
								<p><b><?php echo($user); ?></b> was named <b><?php echo($name); ?></b></p>
								<table>
									<tr class="head">
										<td>Level</td>
										<td>First reached</td>
										<td>Mails sent to this level</td>
									</tr>
									<?php 
										while($stmt -> fetch())
										{
											$time = date('d.m.Y H:i:s', $timestamp);
											?>
												<tr>
													<td><?php echo($level); ?></td>
													<td><?php echo($time); ?></td>
													<td><?php echo($count); ?></td>
												</tr>
											<?php
										}
									?>
								</table>
							<?php
						}
						else
						{
							?>
								<div class="error">
									<h1>Error</h1>
									<p>Such a user doesn't exist</p>
								</div>
							<?php
						}
						$mysqli -> close();
					}
				?>
				<h1>Lookup</h1>
				<p>Enter the checksum or name of a user to look up it's progress</p>
				<form method="GET" action="#">
					<input type="hidden" name="action" value="lookup_user" />
					<label>User to look up: </label>
					<input type="text" name="user" />
					<label>Lookup</label>
					<input type="submit" value="OK" />
				</form>
			</div>
		<?php 
	}
	
	function top_users($index)
	{
		if(empty($index)) $index = 0;
		$end = $index + 50;
		?>
			<div class="headline">
				Top Users
			</div>
			<div class="content">
				<table>
					<tr class="head">
						<td>User</td>
						<td>Level</td>
						<td>Reached</td>
					</tr>
					<?php 
						$mysqli = mopen();
						$stmt = $mysqli -> prepare("SELECT IFNULL(u.Name, m.User), MAX(m.Level), MIN(m.Timestamp) FROM Mails m LEFT JOIN Users u ON u.User = m.User GROUP BY m.User ORDER BY MAX(m.Level) DESC, MIN(m.Timestamp) ASC LIMIT ?, ?");
						$stmt -> bind_param("ii", $index, $end);
						$stmt -> execute();
						$stmt -> bind_result($name, $level, $timestamp);
						while($stmt -> fetch())
						{
							$time = date('d.m.Y H:i:s', $timestamp);
							?>
								<tr>
									<td><?php echo($name); ?></td>
									<td><?php echo($level); ?></td>
									<td><?php echo($time); ?></td>
								</tr>
							<?php
						}
						$stmt -> close();
					?>
				</table>
			</div>
		<?php
		$mysqli -> close();
	}
?>